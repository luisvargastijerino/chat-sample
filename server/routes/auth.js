import User from '../models/user'
import jwt from "jsonwebtoken";
import express from "express";
import config from '../config/database'

export default express.Router()
  .post('/signup', function (req, res) {
    if (!req.body.username || !req.body.password) {
      res.status(406).send('Please pass username and password.');
    } else {
      const newUser = new User({
        username: req.body.username,
        password: req.body.password
      });
      // save the user
      newUser.save(function (err) {
        if (err) {
          return res.status(406).send( 'Username already exists.');
        }
        res.json({message: 'Successful created new user.'});
      });
    }
  })
  .post('/signin', function (req, res) {
    User.findOne({
      username: req.body.username
    }, function (err, user) {
      if (err) throw err;

      if (!user) {
        res.status(401).send('Authentication failed. User not found.');
      } else {
        // check if password matches
        user.comparePassword(req.body.password, (err, isMatch) => {
          if (isMatch && !err) {
            // if user is found and password is right create a token
            const token = jwt.sign(user.toJSON(), config.secret);
            // return the information including token as JSON
            res.json({username: user.username, token: 'bearer ' + token});
          } else {
            res.status(401).send('Authentication failed. Wrong password.');
          }
        });
      }
    });
  })
