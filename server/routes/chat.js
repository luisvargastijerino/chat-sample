import express from "express";
import Chat from "../models/chat";
import passport from "passport";

export default express.Router()
  /* GET ALL CHATS BY ROOM */
  .get('/:room', passport.authenticate('jwt', {session: false}), (req, res, next) => {
    Chat.find({room: req.params.room}, (err, chats) => {
      if (err) return next(err);
      res.json(chats);
    });
  })

  /* SAVE CHAT */
  .post('/', passport.authenticate('jwt', {session: false}), (req, res, next) => {
    Chat.create(req.body, (err, post) => {
      if (err) return next(err);
      res.json(post);
    });
  });

