import debug0 from "debug";
import http from "http";
import express from "express";
import path from "path";
import logger from "morgan";
import bodyParser from "body-parser";
import mongoose from "mongoose";
import chat from "./routes/chat";
import config from './config/database';
import passport from "passport";
import configPassport from "./config/passport";
import auth from "./routes/auth";

configPassport(passport);

const debug = debug0('mean-app:server');

const port = 3000;
const app = express();

/**
 * Create HTTP server.
 */
const server = http.createServer(app)

/**
 * Listen on provided port, on all network interfaces.
 */

  .listen(port)
  .on('error', error => {
    if (error.syscall !== 'listen') {
      throw error;
    }

    const bind = typeof port === 'string'
      ? 'Pipe ' + port
      : 'Port ' + port;

    // handle specific listen errors with friendly messages
    switch (error.code) {
      case 'EACCES':
        console.error(bind + ' requires elevated privileges');
        process.exit(1);
        break;
      case 'EADDRINUSE':
        console.error(bind + ' is already in use');
        process.exit(1);
        break;
      default:
        throw error;
    }
  })
  .on('listening', () => {
    const addr = server.address();
    const bind = typeof addr === 'string'
      ? 'pipe ' + addr
      : 'port ' + addr.port;
    debug('Listening on ' + bind);
  });

const io = require('socket.io')(server);

// socket io
io.on('connection', socket => {
  console.log('User connected');
  socket.on('disconnect', () => {
    console.log('User disconnected');
  });
  socket.on('save-message', data => {
    console.log(data);
    io.emit('new-message', {message: data});
  });
});

mongoose.connect(config.database)
  .then(() => console.log('connection successful'))
  .catch(err => console.log(err));

app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({'extended': 'false'}));
app.use(express.static(path.join(__dirname, '..', 'www')));

app.use('/api/chat', chat);
app.use('/api', auth);
app.get('*', (req, res) => res.redirect('/'));

// catch 404 and forward to error handler
app.use(function (req, res, next) {
  const err = new Error('Not Found');
  err.status = 404;
  next(err);
});

// error handler
app.use(function (err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.render('error');
});
