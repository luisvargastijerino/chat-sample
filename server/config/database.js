
const mongoUrl = process.env.ENVIRONMENT === 'prod' ? 'mongo' : 'localhost';

export default {
  secret: 'chat-sample-secret',
  database: `mongodb://${mongoUrl}/chat-sample`
};
