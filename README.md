# Chat Sample

This is a simple chat application which uses:

* nodejs
* express
* mongodb
* mongoose
* ionic
* passport
* jwt

# Run the project

If `docker` and `docker-compose` are installed then run next command:

```bash
docker-compose up node_serve
```

If not then install `mongodb` and `nodejs` then run next commands:

```bash
npm i && npm run build && npm run start
```
