export interface Chat {
  room: String,
  username: String,
  message: String,
  updated_at: Date
}
