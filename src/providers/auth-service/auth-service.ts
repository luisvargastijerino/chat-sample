import {Injectable} from '@angular/core';
import {Observable} from "rxjs/Observable";
import {User} from "../../models/user";
import {HttpClient} from "@angular/common/http";
import 'rxjs/operator/toPromise'

@Injectable()
export class AuthService {
  currentUser: User;

  constructor(private http: HttpClient) {
  }

  async login(credentials) {
    if (credentials.username === null || credentials.password === null) {
      return Observable.throw("Please insert credentials");
    } else {
      this.currentUser = await this.http.post<User>('/api/signin', credentials).toPromise();
      return this.currentUser;
    }
  }

  public register(credentials) {
    if (credentials.username === null || credentials.password === null) {
      return Observable.throw("Please insert credentials");
    } else {
      // At this point store the credentials to your backend!
      return this.http.post('api/signup', credentials).toPromise();
    }
  }

  public logout() {
    this.currentUser = null;
  }
}
