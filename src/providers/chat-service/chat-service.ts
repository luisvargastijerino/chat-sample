import {HttpClient} from '@angular/common/http';
import {Injectable} from '@angular/core';
import {AuthService} from "../auth-service/auth-service";
import 'rxjs/operator/toPromise';
import {Chat} from "../../models/chat";

@Injectable()
export class ChatService {

  constructor(public http: HttpClient, private auth: AuthService) {
  }

  getChatByRoom(room): Promise<Chat[]> {
    let options = {headers: {Authorization: this.auth.currentUser.token}};
    return this.http.get<Chat[]>('/api/chat/' + room, options).toPromise();
  }

  saveChat(data) {
    let options = {headers: {Authorization: this.auth.currentUser.token}};
    return this.http.post('/api/chat', data, options).toPromise();
  }
}
