import {Component} from '@angular/core';
import {AlertController, Loading, LoadingController, NavController} from 'ionic-angular';
import {AuthService} from '../../providers/auth-service/auth-service';
import {ChatPage} from "../chat/chat";
import {RegisterPage} from "../register/register";

@Component({
  selector: 'page-login',
  templateUrl: 'login.html',
})
export class LoginPage {
  loading: Loading;
  credentials = {username: '', password: '', room: ''};

  constructor(private nav: NavController,
              private auth: AuthService,
              private alertCtrl: AlertController,
              private loadingCtrl: LoadingController) {
  }

  public createAccount() {
    this.nav.push(RegisterPage);
  }

  async login() {
    this.showLoading();
    try {
      let allowed = await this.auth.login(this.credentials);
      if (allowed) {
        let params = {room: this.credentials.room};
        this.nav.setRoot(ChatPage, params);
      } else {
        this.showError("Access Denied");
      }
    } catch (error) {
      this.showError(error.error);
    }
  }

  showLoading() {
    this.loading = this.loadingCtrl.create({
      content: 'Please wait...',
      dismissOnPageChange: true
    });
    this.loading.present();
  }

  showError(text) {
    this.loading.dismiss();

    let alert = this.alertCtrl.create({
      title: 'Fail',
      subTitle: text,
      buttons: ['OK']
    });
    alert.present();
  }
}
