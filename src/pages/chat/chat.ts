import {AfterViewChecked, Component, ElementRef, OnInit, ViewChild} from '@angular/core';
import {IonicPage, NavController, NavParams} from 'ionic-angular';
import {AuthService} from "../../providers/auth-service/auth-service";
import {User} from "../../models/user";
import * as io from "socket.io-client";
import {ChatService} from "../../providers/chat-service/chat-service";
import {LoginPage} from "../login/login";
import {Chat} from "../../models/chat";

/**
 * Generated class for the ChatPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-chat',
  templateUrl: 'chat.html',
})
export class ChatPage implements OnInit, AfterViewChecked {

  room: string;
  user: User;
  @ViewChild('scrollMe') private myScrollContainer: ElementRef;

  chats: Chat[];
  joined = false;
  msgData = {room: '', username: '', message: ''};
  socket = io('http://localhost:3000');


  constructor(public navCtrl: NavController,
              public navParams: NavParams,
              private auth: AuthService,
              private chatService: ChatService) {
  }

  async ngOnInit() {
    this.room = this.navParams.get('room');
    this.user = this.auth.currentUser;
    if (this.user) {
      await this.getChatByRoom(this.room);
      this.msgData = {room: this.room, username: this.user.username, message: ''};
      this.joined = true;
      this.scrollToBottom();
    }
    this.socket.on('new-message', (data) => {
      if (data.message.room === this.room) {
        if (this.chats) {
          this.chats.push(data.message);
        }
        this.msgData = {room: this.room, username: this.user.username, message: ''};
        this.scrollToBottom();
      }
    });
  }

  ngAfterViewChecked() {
    this.scrollToBottom();
  }

  scrollToBottom(): void {
    try {
      this.myScrollContainer.nativeElement.scrollTop = this.myScrollContainer.nativeElement.scrollHeight;
    } catch (err) {
    }
  }

  async getChatByRoom(room) {
    try {
      this.chats = await this.chatService.getChatByRoom(room);
    } catch (err) {
      console.log(err);
    }
  }

  sendMessage() {
    this.chatService.saveChat(this.msgData).then((result) => {
      this.socket.emit('save-message', result);
    }, (err) => {
      console.log(err);
    });
  }

  logout() {
    this.auth.logout();
    this.navCtrl.setRoot(LoginPage);
  }
}
