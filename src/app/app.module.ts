import {BrowserModule} from '@angular/platform-browser';
import {ErrorHandler, NgModule} from '@angular/core';
import {IonicApp, IonicErrorHandler, IonicModule} from 'ionic-angular';
import {SplashScreen} from '@ionic-native/splash-screen';
import {StatusBar} from '@ionic-native/status-bar';

import {MyApp} from './app.component';
import {AuthService} from '../providers/auth-service/auth-service';
import {ChatPageModule} from "../pages/chat/chat.module";
import {LoginPageModule} from "../pages/login/login.module";
import {ChatService} from '../providers/chat-service/chat-service';
import {HttpClientModule} from "@angular/common/http";
import {RegisterPageModule} from "../pages/register/register.module";

@NgModule({
  declarations: [
    MyApp
  ],
  imports: [
    BrowserModule,
    LoginPageModule,
    ChatPageModule,
    RegisterPageModule,
    HttpClientModule,
    IonicModule.forRoot(MyApp)
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp
  ],
  providers: [
    StatusBar,
    SplashScreen,
    {provide: ErrorHandler, useClass: IonicErrorHandler},
    AuthService,
    ChatService,
  ]
})
export class AppModule {
}
